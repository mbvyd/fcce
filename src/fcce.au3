#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=C:\Program Files (x86)\AutoIt3\Icons\au3.ico
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_Res_ProductName=fcce
#AutoIt3Wrapper_Res_Language=1049
#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         https://gitlab.com/intraum

 Script Function:
	Обработка выгрузок Custom Extraction из Screaming Frog

	1) Склейка всех экземпляров одного экстрактора -- чтобы они стали располагаться в одной колонке, через заданный разделитель

	2) подсчёт экземпляров каждого экстрактора на каждом URL'е

#ce ----------------------------------------------------------------------------


#Region ; зависимости

#include <Array.au3>
#include <File.au3>
#include <Excel.au3>
#include <Date.au3>
#include "zavisimosti/FileAux.au3"
#include "zavisimosti/StringAux.au3"
#include "zavisimosti/StringToArray2D.au3"
#include "zavisimosti/GUIAux.au3"
#include <WinAPIDlg.au3>
#include <StaticConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <GuiButton.au3>
#include <EditConstants.au3>
#include <ProgressConstants.au3>

#EndRegion ; зависимости


#Region ; настройки AutoIt

Opt("GUIOnEventMode", 1)

#EndRegion ; настройки AutoIt


#Region ; обычные глобальные переменные

Global Const $esMsgBoxTitle = "Обработчик Custom Extraction"

#EndRegion ; обычные глобальные переменные


#Region ; класс настроек

Global Const $esSettingsGuid = "1b9c1665-d13d-4797-b8fb-2c8c554b67ee"

Global Enum _
		$eiSett_Id, _               ; GUID
		$eiSett_ConfigPath, _       ; путь к файлу настроек
		$eiSett_SectMain, _         ; секция main
		$eiSett_KeyFile, _          ; ключ файла
		$eiSett_FilePath, _         ; имя файла экспорта Фрога
		$eiSett_KeySep, _           ; ключ разделителя
		$eiSett_Sep, _              ; разделитель сущностей экстрактора, собранных в одну колонку
		$eiSett_KeyCount, _         ; ключ счётчиков
		$eiSett_Count, _            ; считать ли встречаемость экстрактора
		$eiSett_Size                ; размер массива


; конструктор

Func Settings()

	Local $hSettings[$eiSett_Size]

	$hSettings[$eiSett_Id] = $esSettingsGuid

	_Settings_SetConfigPath($hSettings)

	$hSettings[$eiSett_SectMain] = 'main'
	$hSettings[$eiSett_KeyFile] = 'file'
	$hSettings[$eiSett_KeySep] = 'sep'
	$hSettings[$eiSett_KeyCount] = 'count'

	Return $hSettings

EndFunc   ;==>Settings


; для инициализации файла настроек;
; здесь и далее -- ф-ции "классов" с префиксом в виде нижнего подчёркивания
; являются "частными", т.е. НЕ предназначены для вызова извне

Func _Settings_SetConfigPath(ByRef $hSettings)

	Local $sFilePath = @ScriptDir & '\' _
			 & StringTrimRight(@ScriptName, StringLen(@ScriptName) _
			 - StringInStr(@ScriptName, '.', Default, -1)) & 'ini'

	$hSettings[$eiSett_ConfigPath] = $sFilePath

EndFunc   ;==>_Settings_SetConfigPath


; здесь и далее - частные методы _IsObject для проверок, тот ли объект

Func _Settings_IsObject(ByRef Const $hSettings)

	Return UBound($hSettings) = $eiSett_Size And _
			$hSettings[$eiSett_Id] == $esSettingsGuid

EndFunc   ;==>_Settings_IsObject


; для установки файла экспорта Фрога

Func _Settings_SetFilePath(ByRef $hSettings)

	$hSettings[$eiSett_FilePath] = IniRead _
			($hSettings[$eiSett_ConfigPath], _
			$hSettings[$eiSett_SectMain], _
			$hSettings[$eiSett_KeyFile], '')

EndFunc   ;==>_Settings_SetFilePath


; для установки разделителя элементов, собираемых в одну колонку

Func _Settings_SetSeparator(ByRef $hSettings)

	$hSettings[$eiSett_Sep] = IniRead _
			($hSettings[$eiSett_ConfigPath], _
			$hSettings[$eiSett_SectMain], _
			$hSettings[$eiSett_KeySep], '')

EndFunc   ;==>_Settings_SetSeparator


Func _Settings_SetTrueFalse(ByRef $hSettings, $index)

	Local $sKey

	; заготовка на случай, если будут ещё соотв. настройки

	Switch $index

		Case $eiSett_Count
			$sKey = $hSettings[$eiSett_KeyCount]

	EndSwitch

	Local $iVal = Int(IniRead($hSettings[$eiSett_ConfigPath], _
			$hSettings[$eiSett_SectMain], $sKey, '0'))

	Local $bCalculated = ($iVal = 1) ? True : False

	$hSettings[$index] = $bCalculated

EndFunc   ;==>_Settings_SetTrueFalse


Func Settings_SetAll(ByRef $hSettings)

	If Not _Settings_IsObject($hSettings) Then Return

	_Settings_SetFilePath($hSettings)
	_Settings_SetSeparator($hSettings)
	_Settings_SetTrueFalse($hSettings, $eiSett_Count)

EndFunc   ;==>Settings_SetAll


Func _Settings_Get(ByRef Const $hSettings, $index)

	Return _Settings_IsObject($hSettings) ? _
			$hSettings[$index] : Null

EndFunc   ;==>_Settings_Get


Func Settings_WriteItem(ByRef Const $hSettings, $indexKey, $vValue)

	If Not _Settings_IsObject($hSettings) Then Return

	Local $indexData

	Switch $indexKey

		Case $eiSett_KeyFile
			$indexData = $eiSett_FilePath

		Case $eiSett_KeySep
			$indexData = $eiSett_Sep

		Case $eiSett_KeyCount
			$indexData = $eiSett_Count

	EndSwitch

	; нет смысла переписывать настройки, если значение оттуда
	; совпадает с текущим переданным

	If _Settings_Get($hSettings, $indexData) = $vValue Then Return

	IniWrite($hSettings[$eiSett_ConfigPath], _
			$hSettings[$eiSett_SectMain], _
			$hSettings[$indexKey], $vValue)

EndFunc   ;==>Settings_WriteItem


; файл настроек создаётся во время работы

Func Settings_CreateFile(ByRef Const $hSettings)

	If Not _Settings_IsObject($hSettings) Then Return

	Local $sFileContents = '[' & $hSettings[$eiSett_SectMain] & ']'
	$sFileContents &= @CRLF
	$sFileContents &= $hSettings[$eiSett_KeyFile] & '=' & @CRLF
	$sFileContents &= $hSettings[$eiSett_KeySep] & '=' & @CRLF
	$sFileContents &= $hSettings[$eiSett_KeyCount] & '=' & @CRLF

	; юникод нельзя для файла настроек, иначе криво работает с кириллицей

	Local $hFile = FileOpen _
			($hSettings[$eiSett_ConfigPath], _
			$FO_OVERWRITE + $FO_ANSI)

	FileWrite($hFile, $sFileContents)
	FileClose($hFile)

EndFunc   ;==>Settings_CreateFile

#EndRegion ; класс настроек


#Region ; класс файла экспорта Лягушки

; "объект" в виде словаря, а не массива, чтобы избежать работы с массивами массивов;
; как-то пробовал, но ушёл от этого потому что вроде слишком тормозилось выполнение;
; хотя и с массивами внутри словарей вроде тоже сталкивался с проблемами скорости,
; поэтому, дело предпочтения, всё равно внутренним массивам присваиваются переменные
; перед работой

Global Const $esCsvSrcGuid = "815a218e-40f8-45d7-9420-3eda48e1db86"

Global Enum _
		$eiCsvSrc_Id, _              ; GUID
		$eiCsvSrc_FilePath, _        ; путь к файлу
		$eiCsvSrc_Sep, _             ; разделитель, заданный пользователем
		$eiCsvSrc_ComplexSep, _      ; сложный разделитель для надёжного разбития на массивы
		$eiCsvSrc_BigArray, _        ; массив основных (всех) данных
		$eiCsvSrc_RangesArr          ; массив диапазонов колонок на удаление


; конструктор

Func CsvSrc()

	Local $hSrcCsv = ObjCreate("Scripting.Dictionary")

	$hSrcCsv.Add($eiCsvSrc_Id, $esCsvSrcGuid)

	$hSrcCsv.Add($eiCsvSrc_ComplexSep, "^--||--^")

	Return $hSrcCsv

EndFunc   ;==>CsvSrc


Func _CsvSrc_IsObject(ByRef Const $hSrcCsv)

	Return $hSrcCsv.Item($eiCsvSrc_Id) == $esCsvSrcGuid

EndFunc   ;==>_CsvSrc_IsObject


Func CsvSrc_SetFilePath(ByRef $hSrcCsv, $sFilePath)

	If Not _CsvSrc_IsObject($hSrcCsv) Then Return

	If Not FileExists($sFilePath) Then Return _
			SetError(1, $MB_ICONWARNING, "Файл " _
			 & $sFilePath & " не существует")

	Local $sExtension = _StringGetFileExt($sFilePath)

	If $sExtension <> "csv" Then Return SetError(2, _
			$MB_ICONWARNING, "Расширение файла " _
			 & $sFilePath & " НЕ csv")

	$hSrcCsv.Add($eiCsvSrc_FilePath, $sFilePath)

EndFunc   ;==>CsvSrc_SetFilePath


Func Cvs_SetSeparator(ByRef $hSrcCsv, $sSeparator)

	If Not _CsvSrc_IsObject($hSrcCsv) Then Return

	If $sSeparator = '","' Or $sSeparator = '";"' Then _
			Return SetError(1, $MB_ICONWARNING, _
			"Разделитель " & $sSeparator & " нельзя использовать, " & _
			"потому что он используется для разделения колонок в CSV")

	$hSrcCsv.Add($eiCsvSrc_Sep, $sSeparator)

EndFunc   ;==>Cvs_SetSeparator


Func CsvSrc_Read(ByRef $hSrcCsv)

	If Not _CsvSrc_IsObject($hSrcCsv) Then Return

	Local $sFileContents
	Local $aArray

	With $hSrcCsv

		_ReadFileNoRet($sFileContents, .Item($eiCsvSrc_FilePath))

		If @error Then Return SetError(1, $MB_ICONERROR, _
				"Не получилось прочитать файл " & _
				.Item($eiCsvSrc_FilePath) & " (" & @error & ")")

		; здесь разделители, используемые Лягушкой

		_StringToArray2D($sFileContents, $aArray, '"' & @CRLF & '"', '","')

		If @error Then Return SetError(2, $MB_ICONERROR, _
				"Не получилось разбить содержимое файла " _
				 & .Item($eiCsvSrc_FilePath) & " в массив")

		; вероятно, уже обработанный или неподходящий файл

		If UBound($aArray, 2) < 2 Then Return SetError(3, _
				$MB_ICONWARNING, "Не получилось нормально разобрать" _
				 & " файл " & .Item($eiCsvSrc_FilePath) & " - возможно," _
				 & " уже обработан или неподходящего формата")
	EndWith

	; убираем кавычку перед заголовком первой (нулевой) колонки

	$aArray[1][0] = StringRegExpReplace($aArray[1][0], '^"', '')

	; и завершающую кавычку в последней (нижней правой) ячейке

	$aArray[$aArray[0][0]][$aArray[0][1]] = StringRegExpReplace _
			($aArray[$aArray[0][0]][$aArray[0][1]], '"$', '')

	$hSrcCsv.Add($eiCsvSrc_BigArray, $aArray)

EndFunc   ;==>CsvSrc_Read


; получить диапазоны колонок на удаление/объединение

Func CsvSrc_SetColRanges(ByRef $hSrcCsv)

	If Not _CsvSrc_IsObject($hSrcCsv) Then Return

	Local $aArray = $hSrcCsv.Item($eiCsvSrc_BigArray)

	Local $aRanges[1]
	Local $iEndRange = -1

	; выясняем, какие колонки должны быть удалены, основываясь на
	; названиях колонок;
	; пропускаем первые 3 колонки, т.к. не являются экстракторами;
	; обратный порядок без какой-то особенной причины, но так удобнее,
	; плюс, удаляться колонки будут, начиная с конца

	For $i = $aArray[0][1] To 3 Step -1

		If _CsvSrc_GetExtractorName($aArray[1][$i]) = _
				_CsvSrc_GetExtractorName($aArray[1][$i - 1]) Then

			If $iEndRange < 0 Then $iEndRange = $i

		Else
			; только если инициализирована конечная граница

			If $iEndRange > 0 Then

				; производим запись диапазона колонок
				; в обратном порядке

				$aRanges[0] = _ArrayAdd($aRanges, $iEndRange & '-' & $i)

				; обнуляем конечную границу, чтобы далее
				; её можно было опять инициализировать

				$iEndRange = -1

			EndIf
		EndIf
	Next

	; если каждый экстрактор встречается не более одного раза на урле,
	; нечего обрабатывать в файле

	If UBound($aRanges) = 1 Then Return SetError(1, $MB_ICONINFORMATION, _
			"Файл " & $hSrcCsv.Item($eiCsvSrc_FilePath) & " не нуждается" _
			 & " в обработке - каждый экстрактор встречается" _
			 & " не более одного раза на URL")

	$hSrcCsv.Add($eiCsvSrc_RangesArr, $aRanges)

EndFunc   ;==>CsvSrc_SetColRanges


; для вырезания из имени экстрактора его счётчика

Func _CsvSrc_GetExtractorName($sColName)

	; если без +1, то остаётся завершающий пробел

	Return StringTrimRight($sColName, StringLen($sColName) _
			 - StringInStr($sColName, ' ', Default, -1) + 1)

EndFunc   ;==>_CsvSrc_GetExtractorName


; последний параметр - считать ли экстракторы
; (если да, то счётчик дописывается в колонку первого экземпляра
; экстрактора через сложный разделитель)

Func CsvSrc_FlattenRanges(ByRef $hSrcCsv, $iCount = 0)

	If Not _CsvSrc_IsObject($hSrcCsv) Then Return

	Local $aOneRange, $aDataOneRange

	Local $aRanges = $hSrcCsv.Item($eiCsvSrc_RangesArr)

	Local $aMain = $hSrcCsv.Item($eiCsvSrc_BigArray)

	For $i = 1 To $aRanges[0]

		_CsvSrc_InitOneRange($aOneRange, $aDataOneRange, _
				$aMain[0][0] + 1, $aRanges[$i])

		; обходим колонки из одного диапазона

		For $j = 1 To $aOneRange[0]

			_CsvSrc_GetOneColData($aMain, $aOneRange, $aDataOneRange, _
					$j, $hSrcCsv.Item($eiCsvSrc_ComplexSep))

			If $j <> $aOneRange[0] Then

				; удаляем все колонки, кроме колонки первого экземпляра экстрактора

				_ArrayColDelete($aMain, $aOneRange[$j])

			Else
				; перезаписать данные в колонке первого экземпляра экстрактора;
				; т.к. j равно $aOneRange[0], то указываем эту переменную для краткости

				_CsvSrc_WriteRangeData($aMain, $aDataOneRange, _
						$aOneRange[$j], $iCount, $hSrcCsv)
			EndIf
		Next
	Next

	_CsvSrc_UpdateColCounter($aMain, UBound($aMain, 2) - 1)

	$hSrcCsv.Item($eiCsvSrc_BigArray) = $aMain

EndFunc   ;==>CsvSrc_FlattenRanges


; подготовка массива, содержащего номера колонок
; текущего диапазона

Func _CsvSrc_InitOneRange(ByRef $aOneRange, _
		ByRef $aDataOneRange, $iUboundRow, $sOneRange)

	; массив будет содержать данные по одному диапазону
	; для всех строк; хотя строки заголовков не будет,
	; но такого же размера, чтобы номера строк массивов совпадали

	Dim $aDataOneRange[$iUboundRow]

	$aOneRange = StringSplit($sOneRange, '-')

	_CsvSrc_ExpandColRange($aOneRange)

EndFunc   ;==>_CsvSrc_InitOneRange


; преобразование диапазона в массив, где один элемент
; будет содержать номер одной колонки из диапазона

Func _CsvSrc_ExpandColRange(ByRef $aOneRange)

	; число колонок, которые принадлежат диапазону;
	; при разности не учитывается один элемент, поэтому +1;
	; диапазон в обратном порядке, поэтому от первого значения
	; отнимаем второе

	Local $iTotalCols = $aOneRange[1] - $aOneRange[2] + 1

	Local $aNew[$iTotalCols + 1]

	$aNew[0] = $iTotalCols

	; первая (последняя в диапазоне) колонка для работы

	Local $iCol = $aOneRange[1]

	For $i = 1 To $iTotalCols

		$aNew[$i] = $iCol

		; диапазон в обратном порядке, поэтому сдвигаем назад (влево)

		$iCol -= 1
	Next

	$aOneRange = $aNew

EndFunc   ;==>_CsvSrc_ExpandColRange


; получить данные всех строк из одной колонки
; заданного диапазона

Func _CsvSrc_GetOneColData(ByRef Const $aMain, ByRef Const $aOneRange, _
		ByRef $aDataOneRange, $iCol, $sComplexSep)

	; пропускаем счётчик и строку заголовков

	For $i = 2 To $aMain[0][0]

		; ячейки, не содержащие данных, надо исключить из обработки

		If $aMain[$i][$aOneRange[$iCol]] = '' Then ContinueLoop

		; не страшно, что завершать будет разделитель, потом от него
		; избавимся, зато так единообразная обработка всех ячеек

		$aDataOneRange[$i] &= $aMain[$i][$aOneRange[$iCol]] & $sComplexSep
	Next
EndFunc   ;==>_CsvSrc_GetOneColData


; для записи данных одного диапазона в колонку первого экземпляра экстрактора

Func _CsvSrc_WriteRangeData(ByRef $aMain, ByRef Const $aDataOneRange, _
		$iCol, $iCount, ByRef Const $hSrcCsv)

	Local $aOneCell

	; пропускаем счётчик и строку заголовков

	For $i = 2 To $aMain[0][0]

		; отсутствие каких-либо данных говорит о том, что в колонке
		; первого экземпляра экстрактора на урле тоже пустота,
		; и нет смысла переписывать пустоту пустотой

		If $aDataOneRange[$i] = '' Then ContinueLoop

		; преобразуем гирлянду подклеек в массив

		$aOneCell = StringSplit($aDataOneRange[$i], _
				$hSrcCsv.Item($eiCsvSrc_ComplexSep), _
				$STR_ENTIRESPLIT)

		; из-за последнего разделителя последний элемент пустой,
		; избавляемся от него

		ReDim $aOneCell[$aOneCell[0]]

		; счётчик после изменения размера тоже надо поправить

		$aOneCell[0] = $aOneCell[0] - 1

		; данные идут от последней колонки диапазона к первой,
		; поэтому их надо обратить, пропуская счётчик

		_ArrayReverse($aOneCell, 1)

		; пропускаем счётчик и используем заданный юзером разделитель

		$aMain[$i][$iCol] = _ArrayToString _
				($aOneCell, $hSrcCsv.Item($eiCsvSrc_Sep), 1)

		; счётчик дописываем при необходимости; опять сложный разделитель,
		; чтобы впоследствии корректно разбить данные

		If $iCount Then $aMain[$i][$iCol] &= _
				$hSrcCsv.Item($eiCsvSrc_ComplexSep) & $aOneCell[0]
	Next
EndFunc   ;==>_CsvSrc_WriteRangeData


; для обновления счётчика колонок

Func _CsvSrc_UpdateColCounter(ByRef $aMain, $i)

	$aMain[0][1] = $i

EndFunc   ;==>_CsvSrc_UpdateColCounter


; добавить колонки счётчиков экземпляров экстракторов

Func CsvSrc_AddColsCounters(ByRef $hSrcCsv)

	If Not _CsvSrc_IsObject($hSrcCsv) Then Return

	Local $aMain = $hSrcCsv.Item($eiCsvSrc_BigArray)

	Local $bAddCol

	; обход с последней колонки, чтобы добавляемые не сбивали
	; номера колонок; 3-я -- последняя колонка с данными, левее
	; идут служебные колонки выгрузки (не экстракторы)

	For $i = $aMain[0][1] To 3 Step -1

		; при начале работы с каждой колонкой требуется новая колонка счётчиков

		$bAddCol = True

		; обходим колонку построчно, начиная со строки начала полезных данных

		For $j = 2 To $aMain[0][0]

			_CsvSrc_CellCheckSep($hSrcCsv, $aMain, $j, $i, $bAddCol)
		Next
	Next

	$hSrcCsv.Item($eiCsvSrc_BigArray) = $aMain

EndFunc   ;==>CsvSrc_AddColsCounters


; обработка ячейки под ключ при добавлении счётчиков

Func _CsvSrc_CellCheckSep(ByRef Const $hSrcCsv, ByRef $aMain, _
		$iRow, $iCol, ByRef $bAddCol)

	; пустые ячейки и без сложного разделителя пропускаем

	If $aMain[$iRow][$iCol] = '' Then Return

	If Not StringInStr($aMain[$iRow][$iCol], _
			$hSrcCsv.Item($eiCsvSrc_ComplexSep)) Then Return

	If $bAddCol Then _CsvSrc_AddColCounters($aMain, $bAddCol, $iCol)

	_CsvSrc_PlaceCounter($hSrcCsv, $aMain, $iRow, $iCol)

EndFunc   ;==>_CsvSrc_CellCheckSep


; добавить колонку счётчиков

Func _CsvSrc_AddColCounters(ByRef $aMain, ByRef $bAddCol, $iCol)

	; после колонки экземпляра экстрактора

	Local $iUboundCol = _ArrayColInsert($aMain, $iCol + 1)

	_CsvSrc_UpdateColCounter($aMain, $iUboundCol - 1)

	; отменить добавление колонок при работе с данной

	$bAddCol = False

	; задаём имя колонки; первый ряд -- строка заголовков

	$aMain[1][$iCol + 1] = _CsvSrc_GetExtractorName _
			($aMain[1][$iCol]) & ' #'

EndFunc   ;==>_CsvSrc_AddColCounters


; установить счётчик для одного урла (строки)
; и перезаписать соотв. основные данные, убрав из них счётчик

Func _CsvSrc_PlaceCounter(ByRef Const $hSrcCsv, ByRef $aMain, $iRow, $iCol)

	Local $aArray = StringSplit($aMain[$iRow][$iCol], _
			$hSrcCsv.Item($eiCsvSrc_ComplexSep), $STR_ENTIRESPLIT)

	; переписывем данные, но уже без счётчика

	$aMain[$iRow][$iCol] = $aArray[1]

	; записываем в колонку рядом счётчик

	$aMain[$iRow][$iCol + 1] = $aArray[2]

EndFunc   ;==>_CsvSrc_PlaceCounter


; для корректности формата файла необходимы кавычки в начале и в конце каждой строки
; (были вырезаны ранее при распиле файла выгрузки в массив)

Func CsvSrc_RestoreQuotes(ByRef $hSrcCsv)

	If Not _CsvSrc_IsObject($hSrcCsv) Then Return

	Local $aArray = $hSrcCsv.Item($eiCsvSrc_BigArray)

	For $i = 1 To $aArray[0][0]

		; обработка только крайних колонок

		$aArray[$i][0] = '"' & $aArray[$i][0]

		$aArray[$i][$aArray[0][1]] = $aArray[$i][$aArray[0][1]] & '"'
	Next

	$hSrcCsv.Item($eiCsvSrc_BigArray) = $aArray

EndFunc   ;==>CsvSrc_RestoreQuotes

#EndRegion ; класс файла экспорта Лягушки


#Region ; класс файла результатов

Global Const $esCsvResGuid = "bfd3f61a-65a2-47df-9e9a-ce2cbb12797b"

Global Enum _
		$eiCsvRes_Id, _              ; GUID
		$eiCsvRes_FilePath, _        ; путь к файлу
		$eiCsvRes_Size               ; размер массива


; конструктор

Func CsvRes()

	Local $hResCsv[$eiCsvRes_Size]

	$hResCsv[$eiCsvRes_Id] = $esCsvResGuid

	Return $hResCsv

EndFunc   ;==>CsvRes


Func _CsvRes_IsObject(ByRef Const $hResCsv)

	Return UBound($hResCsv) = $eiCsvRes_Size And _
			$hResCsv[$eiCsvRes_Id] == $esCsvResGuid

EndFunc   ;==>_CsvRes_IsObject


Func CsvRes_SetFilePath(ByRef $hResCsv, $sSrcFilePath)

	If Not _CsvRes_IsObject($hResCsv) Then Return

	Local $sNameSrcFile = _GetFileNameFromPath($sSrcFilePath)

	; вырезаем .csv

	$sNameSrcFile = StringTrimRight($sNameSrcFile, 4)

	Local $sResFilePath
	Local $sPathDir = _GetDirPathFromFilePath($sSrcFilePath)

	Local $i = 0
	Local $sAdd

	Do
		$sAdd = '_mod'
		$sAdd &= ($i <> 0) ? '_' & $i : ''

		$i += 1

		$sResFilePath = $sPathDir & '\' & $sNameSrcFile & $sAdd & '.csv'

	Until Not FileExists($sResFilePath)

	$hResCsv[$eiCsvRes_FilePath] = $sResFilePath

EndFunc   ;==>CsvRes_SetFilePath


; запись файла результатов

Func CsvRes_Write(ByRef Const $hResCsv, ByRef Const $aArray)

	If Not _CsvRes_IsObject($hResCsv) Then Return

	Local $vMessRet = _CsvRes_FirstWrite($hResCsv, $aArray)

	If @error Then Return SetError(1, @extended, $vMessRet)

	Local $sFileContents

	$vMessRet = _CsvRes_ReadWrittenFix($hResCsv, $sFileContents)

	If @error Then Return SetError(2, @extended, $vMessRet)

	$vMessRet = _CsvRes_FinalWrite($hResCsv, $sFileContents)

	If @error Then Return SetError(3, @extended, $vMessRet)

EndFunc   ;==>CsvRes_Write


Func _CsvRes_FileOpen(ByRef Const $hResCsv, $iMode)

	; если использовать Анси или УТФ-8 без Бом, тогда в кириллице неизбежно
	; появляются кракозябры - и при просмотре в Npp++ и в Экселе

	Return FileOpen($hResCsv[$eiCsvRes_FilePath], _
			$iMode + $FO_UTF8)

EndFunc   ;==>_CsvRes_FileOpen


Func _CsvRes_FirstWrite(ByRef Const $hResCsv, ByRef Const $aArray)

	Local $hResFile = _CsvRes_FileOpen($hResCsv, $FO_APPEND)

	; разделитель не как у Лягушки, чтобы можно было нормально открыть в экселе;
	; _FileWriteFromArray косячно пишет - завершающую кавычку последней ячейки
	; массива переносит на следующую строку

	_FileWriteFromArray($hResFile, $aArray, 1, Default, '";"')

	If @error Then Return SetError(1, $MB_ICONERROR, _
			"Ошибка (" & @error & ") при записи файла " _
			 & $hResCsv[$eiCsvRes_FilePath])

	FileClose($hResFile)

EndFunc   ;==>_CsvRes_FirstWrite


; исправление косяка после записи с помощью _FileWriteFromArray

Func _CsvRes_ReadWrittenFix(ByRef Const $hResCsv, ByRef $sFileContents)

	; переоткрываем файл на чтение, чтобы выудить и исправить косяки

	Local $hResFile = _CsvRes_FileOpen($hResCsv, $FO_READ)

	$sFileContents = FileRead($hResFile)

	If @error Then Return SetError(1, $MB_ICONERROR, _
			"Ошибка (" & @error & ") при чтении файла " _
			 & $hResCsv[$eiCsvRes_FilePath])

	FileClose($hResFile)

	; при необходимости убрать последнюю пустую строку -
	; 2 символа @CR и @LF, кавычку, и ещё одну пустую строку

	If StringRight($sFileContents, 5) _
			 = @CRLF & '"' & @CRLF Then

		$sFileContents = StringTrimRight($sFileContents, 5)

		; добавить удалённую кавычку и пустую последнюю строку

		$sFileContents &= '"' & @CRLF

	EndIf
EndFunc   ;==>_CsvRes_ReadWrittenFix


; для переписывания файла исправленной версией

Func _CsvRes_FinalWrite(ByRef Const $hResCsv, _
		ByRef Const $sFileContents)

	Local $hResFile = _CsvRes_FileOpen($hResCsv, $FO_OVERWRITE)

	Local $iRet = FileWrite($hResFile, $sFileContents)

	If Not $iRet Then Return SetError(1, $MB_ICONERROR, _
			"Ошибка при перезаписи файла " _
			 & $hResCsv[$eiCsvRes_FilePath])

	FileClose($hResFile)

EndFunc   ;==>_CsvRes_FinalWrite

#EndRegion ; класс файла результатов


#Region ; класс графического интерфейса

; т.к. в ф-ции OnEvent нельзя передавать параметры,
; все текстовые идентификаторы элементов указаны так -
; для получения соотв-х значений из ф-ций OnEvent
; по любому элементу

Global Const _
		$esGui_InpFile = '[CLASS:Edit; INSTANCE:1]', _             ; идентификатор поля указания пути файла
		$esGui_BtnBrowse = '[CLASS:Button; INSTANCE:2]', _         ; идентификатор кнопки окна открытия файла
		$esGui_InpSep = '[CLASS:Edit; INSTANCE:2]', _              ; идентификатор поля разделителя
		$esGui_Chkbox = '[CLASS:Button; INSTANCE:3]', _            ; идентификатор чекбокса счётчиков
		$esGui_BtnStart = '[CLASS:Button; INSTANCE:4]', _          ; идентификатор кнопки старта
		$esGui_Progbar = '[CLASS:msctls_progress32; INSTANCE:1]'   ; индентификатор полосы прогресса


Global Enum _
		$eiGui_Hwnd, _                 ; дескриптор главного окна
		$eiGui_ProgbarId, _            ; числовой идентификатор полосы прогресса
		$eiGui_ChkboxId, _             ; числовой идентификатор чекбокса счётчиков
		$eiGui_FilePath, _             ; путь к CSV файлу
		$eiGui_Sep, _                  ; разделитель
		$eiGui_Count, _                ; состояние чекбокса счётчиков
		$eiGui_Size                    ; размер массива


; конструктор

Func Gui($sFilePath = '', $sSeparator = ',', $iCount = 0)

	Local $hGui
	Local $idChkboxCounters

	$hGui = GUICreate($esMsgBoxTitle, 476, 238, -1, -1, -1, $WS_EX_ACCEPTFILES)
	GUISetOnEvent($GUI_EVENT_CLOSE, "Gui_Exit", $hGui)
	GUISetOnEvent($GUI_EVENT_DROPPED, "_Gui_FileDropped", $hGui)
	GUICtrlCreateGroup("CSV файл Custom Extraction", 20, 15, 436, 79, -1, -1)
	GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
	GUICtrlCreateInput($sFilePath, 41, 44, 348, 27, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $GUI_ENABLE, $GUI_DROPACCEPTED))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, 'Файл экспорта Custom Extraction из Лягушки')
	GUICtrlCreateButton("...", 405, 44, 32, 27, -1, -1)
	GUICtrlSetOnEvent(-1, "Gui_SelectFile")
	GUICtrlSetFont(-1, 14, 400, 0, "MS Sans Serif")
	GUICtrlCreateLabel("Разделитель", 20, 114, 89, 18, -1, -1)
	GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "-2")
	GUICtrlSetTip(-1, "Экземляры экстракторов будут склеены через этот разделитель")
	GUICtrlCreateInput($sSeparator, 112, 110, 63, 27, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
	$idChkboxCounters = GUICtrlCreateCheckbox("Добавить счётчики", 312, 114, 144, 20, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Для каждого URL'а будут посчитаны экземпляры экстракторов")
	ControlCommand($hGui, "", $idChkboxCounters, $iCount = 1 ? "Check" : "UnCheck", "")
	GUICtrlCreateProgress(0, 216, 476, 22, $PBS_MARQUEE, -1)
	GUICtrlSetState(-1, BitOR($GUI_HIDE, $GUI_ENABLE))
	GUICtrlCreateButton("Старт", 194, 162, 91, 30, -1, -1)
	GUICtrlSetOnEvent(-1, "Gui_ExecOps")
	GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")

	Return $hGui

EndFunc   ;==>Gui


Func Gui_Init()

	Local $hSettings = Settings()
	Local $hGui

	If FileExists($hSettings[$eiSett_ConfigPath]) Then

		Settings_SetAll($hSettings)

		$hGui = Gui($hSettings[$eiSett_FilePath], _
				$hSettings[$eiSett_Sep], _
				$hSettings[$eiSett_Count])
	Else
		$hGui = Gui()

	EndIf

	GUISetState(@SW_SHOW, $hGui)

EndFunc   ;==>Gui_Init


Func Gui_Exit()

	Exit

EndFunc   ;==>Gui_Exit


; при перетаскивании файла в поле

Func _Gui_FileDropped()

	GUICtrlSetData(@GUI_DropId, @GUI_DragFile)

EndFunc   ;==>_Gui_FileDropped


Func Gui_SelectFile()

	Local $sCurrDir = _GetDirPathFromFilePath( _
			ControlGetText(@GUI_WinHandle, '', $esGui_InpFile))

	Local $sWinTitle = $esMsgBoxTitle & " - выбор файла"

	Local $sFileTypes = "CSV (*.csv)"

	Local $sSelectedFile = FileOpenDialog($sWinTitle, $sCurrDir, _
			$sFileTypes, 0, '', @GUI_WinHandle)

	; если файл не был выбран (сохраняется прежнее значение поля)
	If @error Then Return

	ControlSetText(@GUI_WinHandle, '', $esGui_InpFile, $sSelectedFile)

EndFunc   ;==>Gui_SelectFile


Func Gui_ExecOps()

	Local $hObjGui = _Gui_MakeObj(@GUI_WinHandle)

	_Gui_ToggleControls($hObjGui)
	_Gui_ToggleProgbar($hObjGui)

	_Gui_CollectData($hObjGui)
	_Gui_SaveDataToFile($hObjGui)

	Local $hSrcCsv = CsvSrc()

	Local $vMessRet = _Gui_ProcessSource($hObjGui, $hSrcCsv)

	If @error Then Return _Gui_OnExecError _
			($hObjGui, $vMessRet, @extended)

	Local $hResCsv = CsvRes()

	$vMessRet = _Gui_ProcessRes($hSrcCsv, $hResCsv)

	If @error Then Return _Gui_OnExecError _
			($hObjGui, $vMessRet, @extended)

	_Gui_ToggleControls($hObjGui)
	_Gui_ToggleProgbar($hObjGui)

	Return MsgBox($MB_ICONINFORMATION, $esMsgBoxTitle, _
			"Работа завершена, файл результатов: " & _
			$hResCsv[$eiCsvRes_FilePath], 0, _
			$hObjGui[$eiGui_Hwnd])

EndFunc   ;==>Gui_ExecOps


; прерывание операций при ошибках для возврата к главному окну

Func _Gui_OnExecError(ByRef Const $hObjGui, _
		ByRef Const $vMessRet, $iComboMB)

	_Gui_ToggleControls($hObjGui)
	_Gui_ToggleProgbar($hObjGui)

	MsgBox($iComboMB, $esMsgBoxTitle, $vMessRet, _
			0, $hObjGui[$eiGui_Hwnd])

EndFunc   ;==>_Gui_OnExecError


; конструктор внутренний

Func _Gui_MakeObj(ByRef Const $hGui)

	; чтобы при каждом нажатии старта не конструировать объект заново

	Local Static $hObjGui[$eiGui_Size]

	Local Static $bInited = False

	If $bInited Then Return $hObjGui

	$hObjGui[$eiGui_Hwnd] = $hGui

	$hObjGui[$eiGui_ProgbarId] = _GUIGetCtrlID($hObjGui[$eiGui_Hwnd], $esGui_Progbar)

	$hObjGui[$eiGui_ChkboxId] = _GUIGetCtrlID($hObjGui[$eiGui_Hwnd], $esGui_Chkbox)

	$bInited = True

	Return $hObjGui

EndFunc   ;==>_Gui_MakeObj


; затемнение и его снятие для элементов; без ф-ций GUI,
; чтобы не делать лишние запросы Винапи

Func _Gui_ToggleControls(ByRef Const $hObjGui)

	; если хотя бы один элемент включен, значит все включены

	If ControlCommand($hObjGui[$eiGui_Hwnd], '', _
			$esGui_InpFile, "IsEnabled") Then

		ControlDisable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_InpFile)

		ControlDisable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_BtnBrowse)

		ControlDisable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_InpSep)

		ControlDisable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_Chkbox)

		ControlDisable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_BtnStart)
	Else
		ControlEnable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_InpFile)

		ControlEnable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_BtnBrowse)

		ControlEnable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_InpSep)

		ControlEnable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_Chkbox)

		ControlEnable($hObjGui[$eiGui_Hwnd], _
				'', $esGui_BtnStart)
	EndIf
EndFunc   ;==>_Gui_ToggleControls


Func _Gui_ToggleProgbar(ByRef Const $hObjGui)

	If Not ControlCommand($hObjGui[$eiGui_Hwnd], '', _
			$esGui_Progbar, "IsVisible") Then

		GUICtrlSetState($hObjGui[$eiGui_ProgbarId], $GUI_SHOW)
		GUICtrlSendMsg($hObjGui[$eiGui_ProgbarId], $PBM_SETMARQUEE, 1, 50)

	Else
		GUICtrlSetState($hObjGui[$eiGui_ProgbarId], $GUI_HIDE)
		GUICtrlSendMsg($hObjGui[$eiGui_ProgbarId], $PBM_SETMARQUEE, 0, 50)

	EndIf
EndFunc   ;==>_Gui_ToggleProgbar


Func _Gui_CollectData(ByRef $hObjGui)

	$hObjGui[$eiGui_FilePath] = ControlGetText _
			($hObjGui[$eiGui_Hwnd], '', $esGui_InpFile)

	$hObjGui[$eiGui_Sep] = ControlGetText _
			($hObjGui[$eiGui_Hwnd], '', $esGui_InpSep)

	$hObjGui[$eiGui_Count] = _
			_GUICtrlChkboxRadRead($hObjGui[$eiGui_ChkboxId])

EndFunc   ;==>_Gui_CollectData


Func _Gui_SaveDataToFile(ByRef Const $hObjGui)

	Local $hSettings = Settings()

	If Not FileExists($hSettings[$eiSett_ConfigPath]) Then

		Settings_CreateFile($hSettings)
	Else
		; считать все настройки, чтобы потом проверить, надо ли их перезаписывать

		Settings_SetAll($hSettings)
	EndIf

	Settings_WriteItem($hSettings, $eiSett_KeyFile, _
			$hObjGui[$eiGui_FilePath])

	Settings_WriteItem($hSettings, $eiSett_KeySep, _
			$hObjGui[$eiGui_Sep])

	Settings_WriteItem($hSettings, $eiSett_KeyCount, _
			$hObjGui[$eiGui_Count])

EndFunc   ;==>_Gui_SaveDataToFile


; источник и результат на первый взгляд лучше относятся к соответствующим
; классам, однако вызов методов других классов в теле основного рабочего метода
; видится ещё более неподходящим;
; лучше вроде бы заизолировать работу с другими объектами в отдельных методах

Func _Gui_ProcessSource(ByRef Const $hObjGui, ByRef $hSrcCsv)

	Local $vMessRet = CsvSrc_SetFilePath($hSrcCsv, _
			$hObjGui[$eiGui_FilePath])

	If @error Then Return SetError(1, @extended, $vMessRet)

	$vMessRet = Cvs_SetSeparator($hSrcCsv, $hObjGui[$eiGui_Sep])

	If @error Then Return SetError(2, @extended, $vMessRet)

	$vMessRet = CsvSrc_Read($hSrcCsv)

	If @error Then Return SetError(3, @extended, $vMessRet)

	CsvSrc_SetColRanges($hSrcCsv)

	If @error Then Return SetError(4, @extended, $vMessRet)

	CsvSrc_FlattenRanges($hSrcCsv, $hObjGui[$eiGui_Count])

	CsvSrc_AddColsCounters($hSrcCsv)

	CsvSrc_RestoreQuotes($hSrcCsv)

EndFunc   ;==>_Gui_ProcessSource


Func _Gui_ProcessRes(ByRef Const $hSrcCsv, ByRef $hResCsv)

	CsvRes_SetFilePath($hResCsv, $hSrcCsv.Item($eiCsvSrc_FilePath))

	Local $aArray = $hSrcCsv.Item($eiCsvSrc_BigArray)

	Local $vMessRet = CsvRes_Write($hResCsv, $aArray)

	If @error Then Return SetError(1, @extended, $vMessRet)

EndFunc   ;==>_Gui_ProcessRes

#EndRegion ; класс графического интерфейса


#Region ; выполнение

Gui_Init()


; простой GUI

While 1

	Sleep(100)
WEnd

#EndRegion ; выполнение
